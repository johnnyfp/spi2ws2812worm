################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/SPI2WS281xFirmware.cpp \
../src/cr_cpp_config.cpp \
../src/cr_startup_lpc11xxlv.cpp 

C_SRCS += \
../src/crp.c 

OBJS += \
./src/SPI2WS281xFirmware.o \
./src/cr_cpp_config.o \
./src/cr_startup_lpc11xxlv.o \
./src/crp.o 

C_DEPS += \
./src/crp.d 

CPP_DEPS += \
./src/SPI2WS281xFirmware.d \
./src/cr_cpp_config.d \
./src/cr_startup_lpc11xxlv.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -D__NEWLIB__ -DNDEBUG -D__CODE_RED -DCORE_M0 -D__USE_CMSIS=CMSIS_CORE_LPC11xxLV -DCPP_USE_HEAP -D__LPC11XXLV__ -I"D:\Programming\Projects\SPI2WS2812-Worm\SPI2WSFirmware\workspace\CMSIS_CORE_LPC11xxLV\inc" -Os -Os -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-rtti -fno-exceptions -mcpu=cortex-m0 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__NEWLIB__ -DNDEBUG -D__CODE_RED -DCORE_M0 -D__USE_CMSIS=CMSIS_CORE_LPC11xxLV -DCPP_USE_HEAP -D__LPC11XXLV__ -I"D:\Programming\Projects\SPI2WS2812-Worm\SPI2WSFirmware\workspace\CMSIS_CORE_LPC11xxLV\inc" -Os -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m0 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


